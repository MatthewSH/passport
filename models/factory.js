'use strict';
module.exports = (sequelize, DataTypes) => {
  const Factory = sequelize.define('Factory', {
    name: DataTypes.STRING,
    children: DataTypes.STRING
  }, {});
  Factory.associate = function(models) {
    // associations can be defined here
  };
  return Factory;
};