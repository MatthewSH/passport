var Vue = require('vue/dist/vue.js');
var axios = require('axios');
var VueAxios = require('vue-axios');

Vue.use(VueAxios, axios)

Vue.component('root', require('./components/Root.vue'));
Vue.component('factory-nodes', require('./components/FactoryNodes.vue'));

const app = new Vue({
    el: '#app',
    runtimeCopm: true,
})