var express = require('express');
var router = express.Router();
var models = require('../models');
const { celebrate, Joi, errors } = require('celebrate');

router.get('/', function(req, res) {
    models.Factory.findAll({
        order: [
            ['createdAt', 'ASC']
        ]
    }).then((factories) => {
        res.send(factories);
    });
});

router.delete('/:factoryId', function (req, res) {
    models.Factory.findByPk(req.params.factoryId).then(factory => {
        return factory.destroy();
    }).then(deletedFactory => {
        res.send({
            error: false
        })
    }).catch(error => {
        res.send({
            error: true,
            body: error,
        })
    });
});

router.patch('/:factoryId', celebrate({
    body: Joi.object().keys({
        name: Joi.optional()
    })
}), function(req, res) {
    models.Factory.findByPk(req.params.factoryId).then(factory => {
        return factory.update({
            name: req.body.name ? req.body.name : 'Factory',
        });
    }).then(factory => {
        res.send({
            error: false,
            factory: factory
        })
    }).catch(error => {
        res.send({
            error: true,
            body: error,
        })
    })
});

router.post('/create', celebrate({
    body: Joi.object().keys({
        name: Joi.optional(),
    })
}), function(req, res) {
    models.Factory.create({
        name: req.body.name ? req.body.name : 'Factory',
    }).then(([factory, created]) => {
        res.send(factory);
    }).catch(error => {
        res.send({});
    });
});

router.post('/:factoryId/generate', celebrate({
    body: Joi.object().keys({
        amount: Joi.number().min(1).max(15).required(),
        min: Joi.number().required(),
        max: Joi.number().required(),
    })
}), function(req, res) {
    let min = req.body.min ? req.body.min : 0;
    let max = req.body.max ? req.body.max : 100;
    let amount = req.body.amount ? req.body.amount : 15;
    let children = [];

    for (let i = 0; i < amount; i++) {
        children[i] = Math.floor(Math.random() * (max - min + 1)) + min;
    }

    models.Factory.findByPk(req.params.factoryId).then(factory => {
        return factory.update({
            children: JSON.stringify(children),
        });
    }).then(factory => {
        res.send({
            error: false,
            factory: factory
        })
    }).catch(error => {
        res.send({
            error: true,
            body: error,
        })
    })
});

router.use(errors());

module.exports = router;
